export default {
  fr: {
    outward: "Aller",
    return: "Retour",
    multipleTimesSlots: "Plusieurs créneaux"
  },
  en: {
    outward: "Outward",
    return: "Return",
    multipleTimesSlots: "Multiple times slots"
  }
}