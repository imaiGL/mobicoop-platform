export default {
  fr: {
    proposals: {
      ongoing: "Annonces en cours",
      archived: "Annonces archivées"
    }
  },
  en: {
    proposals: {
      ongoing: "On going proposals",
      archived: "Archived proposals"
    }
  }
}