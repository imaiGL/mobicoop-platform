export default {
  fr: {
    seat: {
      singular: "place",
      plural: "places"
    },
    potentialCarpooler: {
      singular: "covoitureur potentiel",
      plural: "covoitureurs potentiels"
    }
  },
  en: {
    seat: {
      singular: "seat",
      plural: "seats"
    },
    potentialCarpooler: {
      singular: "potential carpooler",
      plural: "potentials carpoolers"
    }
  }
}